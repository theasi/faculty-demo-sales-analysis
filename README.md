
# Productionisation on the Faculty platform

This repository contains some sample code to demonstrate productionisation on
the Faculty platform. It concentrates on productionisation of the training
process and exporting artifacts.

## Data and problem

Our sample problem aims to classify whether a particular opportunity in a
sales pipeline was won or lost.

The data is originally from [here](https://www.ibm.com/communities/analytics/watson-analytics-blog/sales-win-loss-sample-dataset/).

![](./readme-images/sales-data.png)

The directory `sales_prediction/sales` contains useful functions and classes
that would be developed as part of the exploration and development phase
of data science:

- tools for creating a train-test split of the data
- a scikit-learn pipeline that trains an XGBoost model
- a class called `MetricCalculator` for calculating the performance
  of our model.

## Productionisation of training

Under `sales_prediction/train`, there are two scripts:

- `run_training.py` is the type of script you might have at the end of the
  development phase: it reads data from the local filesystem, trains a model
  and saves a pickle file back to the local filesystem.
- `run_training_mlflow.py` is an adapted version where the training has been
  made more reproducible: we log model hyperparamers, the path to the training 
  data, the full train-test split and the final model to 
  [experiments](https://docs.faculty.ai/user-guide/experiments/index.html) 
  in the platform.

![](/readme-images/training-experiment.png)

The `setup.py` file contains an entrypoint for `run_training.py`. This can then
be integrated into a [job](https://docs.faculty.ai/user-guide/jobs/index.html) 
on the Faculty platform.

![](./readme-images/job-training.png)

## Batch predictions

One common route to automate getting predictions is batch processing. The
consumer uploads unlabelled data, runs a script that calculates predictions for
that data and saves it to, e.g. a CSV. The consumer downloads the CSV.

This can be automated on the Faculty platform. The script
`sales_prediction/predict/run_prediction.py` allows the user to pass in a
path to some unlabelled data on the workspace. The script computes
predictions and saves them, along with the data, as an artifact using
experiment tracking.

![](./readme-images/job-predict.png)

## Creating an API

In `sales_prediction/app`, we create a simple Flask API that returns
predictions for a single record.

During development of the app, we fetch the model dynamically from experiment
tracking using, e.g.:

```py
model = mlflow.sklearn.load_model(
    "trained-model", 
    run_id="5f4f65a5-8e37-4bee-9221-3cc4f9875c84"
)
```

To test the API, run the script in `sales_prediction/test-app/run.py`.

Once ready, the API and the rest of the repository can be bundled into a
Python source distribution by running the following command in the repository
root:

```
python setup.py sdist
```

## Exporting an API as a Docker container

To attach the environment to the API, it can be baked into a Docker image.

We will re-use the API code that was developed in the previous section.
However, we now want to put the model itself into the Docker image, not fetch 
it dynamically at application startup. For this, we change the application 
code to load the model from `/app/model`:

```py
model = mlflow.sklearn.load_model("/app/model")
```

To build the container, make sure you are connected to a Docker server (speak
to a platform support engineer if you are not sure how to do this).

Then, run the script in `/scripts/build $RUN_ID`, replacing `$RUN_ID` with the 
run corresponding to the model you want to use. This will:

1. download the model corresponding to `$RUN_ID`.
2. copy the model and the `sales_prediction` module into a WSGI-NGINX image.
3. save an image as `sales-predictor:$VERSION`, where `$VERSION` concatenates
   the code version and the model version.

To test the container, run it using:

```
docker run -it -p 8080:80 sales-predictor:$VERSION
```

... replacing $VERSION with the version of the container that was built.

Then, open an SSH tunnel to the docker build machine, mapping port 8080
to the same port locally:

```
docker-machine ssh remote-build -f -N -L 8080:127.0.0.1:8080
```

You should now be able to test the API code with
`sales_prediction/test-app/run.py`.