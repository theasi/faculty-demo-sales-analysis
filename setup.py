from setuptools import setup, find_packages

setup(
    name="sales_prediction",
    packages=find_packages(),
    version="0.0.10",
    install_requires=[
        "mlflow",
        "scikit-learn==0.20.3",
        "xgboost",
        "nose",
        "urllib3==1.24",
        "matplotlib",
    ],
    entry_points={
        "console_scripts": [
            "sales-train = sales_prediction.train.run_training:main",
            "sales-train-mlflow = sales_prediction.train.run_training_mlflow:main",
            "sales-predict = sales_prediction.predict.run_prediction:main",
        ]
    },
)
