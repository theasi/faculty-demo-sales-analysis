FROM tiangolo/uwsgi-nginx-flask:python3.6

COPY sales_prediction /app/sales_prediction/
COPY model /app/model/
COPY setup.py deploy/uwsgi.ini deploy/prestart.sh /app/

RUN cd /app && python setup.py install