import mlflow.sklearn
import pandas as pd

from flask import Flask, request, jsonify

import sales_prediction.sales


app = Flask(__name__)


# MODEL = mlflow.sklearn.load_model(
#     "trained-model", run_id="5f4f65a5-8e37-4bee-9221-3cc4f9875c84"
# )

MODEL = mlflow.sklearn.load_model("/app/model")


def predict(record):
    df = pd.DataFrame.from_records([record], columns=MODEL.expected_columns)
    prediction = MODEL.predict(df)[0]
    return sales_prediction.sales.restore_target(prediction)


@app.route("/predict", methods=["POST"])
def run_prediction():
    body = request.get_json()

    if set(body.keys()) != set(MODEL.expected_columns):
        response = jsonify({"error": "Incorrect field(s)"})
        response.status_code = 400
        return response

    prediction = predict(body)
    return jsonify({"prediction": prediction})


if __name__ == "__main__":
    app.run(port=5000, debug=True)
