import argparse
import logging
import uuid
import tempfile
from pathlib import Path

import pandas as pd

import mlflow
import mlflow.sklearn

from sales_prediction import sales


logging.basicConfig(level=logging.INFO)


EXPERIMENT_ID = 1


def create_parser():
    parser = argparse.ArgumentParser(
        'predict',
        description='Run batch predictions for opportunity win probability'
    )
    parser.add_argument('raw', type=Path)
    parser.add_argument('run_id', type=uuid.UUID)
    return parser


def log_metrics(input_df, predictions):
    nrows = input_df.shape[0]
    number_won = (predictions == 'Won').sum()
    number_lost = (predictions == 'Loss').sum()
    mlflow.log_metrics({
        'number_won': number_won,
        'number_lost': number_lost,
        'fraction_won': number_won / nrows,
        'fraction_lost': number_lost / nrows
    })


def save_predictions(modified_df):
    with tempfile.TemporaryDirectory() as tempdir:
        output_path = Path(tempdir) / 'predictions.csv'
        modified_df.to_csv(output_path)
        mlflow.log_artifact(str(output_path))


def run_prediction(input_path, model_run_id):
    with mlflow.start_run(experiment_id=EXPERIMENT_ID) as run:
        mlflow.log_params({
            'run_id': model_run_id,
            'data_path': input_path
        })
        model = mlflow.sklearn.load_model(
            'trained-model', 
            run_id=str(model_run_id)
        )
        logging.info(f'Retrieved model for run ID {model_run_id}.')
        input_df = pd.read_csv(input_path)
        logging.info('Loaded the data.')
        mlflow.log_metric('rows', input_df.shape[0])
        numeric_predictions = pd.Series(model.predict(input_df))
        predictions = sales.restore_target_in_series(numeric_predictions)
        log_metrics(input_df, predictions)
        input_df['Opportunity Result - predicted'] = predictions
        save_predictions(input_df)
        logging.info(f'Saved predictions to run {run.info.run_uuid}.')


def main():
    args = create_parser().parse_args()
    run_prediction(args.raw, args.run_id)


if __name__ == '__main__':
    main()