import os
from collections import namedtuple

import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split

import cloudpickle as pickle

RANDOM_STATE = int(os.getenv('RANDOM_STATE', 221))

SplitData = namedtuple(
    'SplitData', 
    [
        'train_features', 
        'train_target', 
        'test_features', 
        'test_target'
    ]
)

TARGET_VALUES = {
    'Loss': 0,
    'Won': 1
}


def transform_target(series):
    return series.map(TARGET_VALUES)


def transform_target_in_df(df):
    processed_df = df.copy()
    processed_df['Opportunity Result'] = transform_target(df['Opportunity Result'])
    return processed_df


def restore_target(value):
    return {k:v for v, k in TARGET_VALUES.items()}[value]


def restore_target_in_array(array):
    inverse_map = {k:v for v, k in TARGET_VALUES.items()}
    mapped_array = np.vectorize(inverse_map.get)(array)
    return mapped_array


def split_data(input_df):
    cols = [col for col in input_df.columns if col not in ['Opportunity Number', 'Opportunity Result']]
    data = input_df[cols]
    target = input_df['Opportunity Result']

    train_features, test_features, train_target, test_target = train_test_split(
        data, target, test_size=0.25, random_state=RANDOM_STATE)
    return SplitData(train_features, train_target, test_features, test_target)


def save_splits(root_path, split_data):
    if not root_path.exists():
        root_path.mkdir()
    with (root_path / 'train_features.csv').open('w') as f:
        split_data.train_features.to_csv(f, index=False)
    with (root_path / 'test_features.csv').open('w') as f:
        split_data.test_features.to_csv(f, index=False)
    with (root_path / 'train_target.csv').open('w') as f:
        split_data.train_target.to_csv(f, index=False, header=True)
    with (root_path / 'test_target.csv').open('w') as f:
        split_data.test_target.to_csv(f, index=False, header=True)


def load_splits(root_path):
    with (root_path / 'train_features.csv').open() as f:
        train_features = pd.read_csv(f)
    with (root_path / 'test_features.csv').open() as f:
        test_features = pd.read_csv(f)
    with (root_path / 'train_target.csv').open() as f:
        train_target = pd.read_csv(f)
    with (root_path / 'test_target.csv').open() as f:
        test_target = pd.read_csv(f)
    return SplitData(train_features, train_target, test_features, test_target)