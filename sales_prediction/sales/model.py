import os

import cloudpickle as pickle

from sklearn.preprocessing import OrdinalEncoder
from sklearn.compose import make_column_transformer
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import RandomizedSearchCV

from xgboost import XGBClassifier


NUM_CPUS = int(os.getenv('NUM_CPUS', 1))
RANDOM_STATE = int(os.getenv('RANDOM_STATE', 221))


class SalesPredictor:

    def __init__(self, n_estimators=100, n_iter=1):
        categorical_columns = [
            'Supplies Subgroup', 'Region', 'Route To Market',
            'Competitor Type', 'Supplies Group'
        ]
        preprocess = make_column_transformer(
            (OrdinalEncoder(), categorical_columns),
            remainder='passthrough'
        )
        clf = XGBClassifier(n_estimators=n_estimators, nthread=1)
        model = make_pipeline(preprocess, clf)
        params = {
            'xgbclassifier__min_child_weight': [1, 5, 10],
            'xgbclassifier__gamma': [0.5, 1, 1.5, 2, 5],
            'xgbclassifier__subsample': [0.6, 0.8, 1.0],
            'xgbclassifier__colsample_bytree': [0.6, 0.8, 1.0],
            'xgbclassifier__max_depth': [3, 4, 5]
        }

        self._random_search = RandomizedSearchCV(
            model,
            param_distributions=params, 
            n_iter=n_iter,
            scoring='roc_auc', 
            n_jobs=NUM_CPUS,
            cv=3,
            verbose=3, 
            random_state=RANDOM_STATE,
            
        )

    def fit(self, X, y):
        self._random_search.fit(X, y)
        self.expected_columns = X.columns.to_list()

    def predict(self, X):
        if X.columns.to_list() != self.expected_columns:
            raise ValueError('Data does not match expected columns.')
        return self._random_search.predict(X)

    def predict_proba(self, X):
        return self._random_search.predict_proba(X)


def save_trained_model(trained_model, f):
    pickle.dump(trained_model, f)


def load_trained_model(f):
    return pickle.load(f)
