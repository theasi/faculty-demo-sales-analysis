from sklearn.metrics import roc_curve, f1_score, accuracy_score, confusion_matrix


class MetricCalculator:

    def __init__(self, trained_model, test_data, test_target):
        self._trained_model = trained_model
        self._test_data = test_data
        self._test_target = test_target
        self._get_predictions()
        self._get_prediction_probabilities()

    def _get_predictions(self):
        self._predictions = self._trained_model.predict(self._test_data)

    def _get_prediction_probabilities(self):
        self._prediction_probabilities = self._trained_model.predict_proba(
            self._test_data)[:, 1]
    
    def get_confusion_matrix(self):
        return confusion_matrix(self._test_target, self._predictions)

    def get_f1_score(self):
        return f1_score(self._test_target, self._predictions)

    def get_accuracy(self):
        return accuracy_score(self._test_target, self._predictions, normalize=True)

    def get_roc_curve(self):
        x, y, _ = roc_curve(self._test_target, self._prediction_probabilities)
        return (x, y)