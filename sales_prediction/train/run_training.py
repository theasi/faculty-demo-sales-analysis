import argparse
import logging
from pathlib import Path

import pandas as pd

from sales_prediction import sales

logging.basicConfig(level=logging.INFO)


def create_parser():
    parser = argparse.ArgumentParser(
        'train',
        description='Train a model on sales data.'
    )

    parser.add_argument('raw', type=Path)
    parser.add_argument('trained', type=Path)
    parser.add_argument('--iter', type=int, default=1)
    parser.add_argument('--estimators', type=int, default=100)
    return parser


def train(input_path, trained_model_path, n_iter, n_estimators):
    input_df = pd.read_csv(input_path)
    logging.info(f'Read {input_df.shape[0]} rows of input data.')
    processed_df = sales.transform_target_in_df(input_df)
    logging.info('Preprocessed the data.')
    splits = sales.split_data(processed_df)
    logging.info('Created splits.')
    model = sales.SalesPredictor(n_estimators, n_iter)
    model.fit(splits.train_features, splits.train_target)
    logging.info('Model training complete.')
    with trained_model_path.open('wb') as fp:
        sales.save_trained_model(model, fp)
    logging.info('Trained model saved.')
    metric_calculator = sales.MetricCalculator(
        model, splits.test_features, splits.test_target)
    logging.info('Calculated metrics.')
    logging.info(f'Confusion matrix: \n{metric_calculator.get_confusion_matrix()}')
    logging.info(f'F1 score: {metric_calculator.get_f1_score()}')
    logging.info(f'Accuracy: {metric_calculator.get_accuracy()}')


def main():
    args = create_parser().parse_args()
    train(args.raw, args.trained, args.iter, args.estimators)


if __name__ == '__main__':
    main()