import argparse
import logging
from pathlib import Path
import tempfile

import mlflow
import mlflow.sklearn
import matplotlib.pyplot as plt
import pandas as pd

from sales_prediction import sales


logging.basicConfig(level=logging.INFO)


EXPERIMENT_ID = 1


def create_parser():
    parser = argparse.ArgumentParser(
        'train',
        description='Train a model on sales data.'
    )

    parser.add_argument('raw', type=Path)
    parser.add_argument('--estimators', type=int, default=100)
    parser.add_argument('--iter', type=int, default=3)
    return parser


def log_params(input_path, n_estimators, n_iter):
    mlflow.log_params({
        'input_path': input_path,
        'n_estimators': n_estimators,
        'random_state': sales.RANDOM_STATE,
        'n_cpus': sales.NUM_CPUS,
        'n_iter': n_iter
    })


def log_metrics(metric_calculator):
    confusion_matrix = metric_calculator.get_confusion_matrix()
    logging.info('Calculated metrics.')
    logging.info(f'Confusion matrix: \n{confusion_matrix}')
    logging.info(f'F1 score: {metric_calculator.get_f1_score()}')
    logging.info(f'Accuracy: {metric_calculator.get_accuracy()}')
    mlflow.log_metrics({
        'accuracy': metric_calculator.get_accuracy(),
        'F1': metric_calculator.get_f1_score(),
        'cm_tp': confusion_matrix[0][0],
        'cm_tn': confusion_matrix[1][1],
        'cm_fp': confusion_matrix[0][1],
        'cm_fn': confusion_matrix[1][0]
    })
    roc_curve = metric_calculator.get_roc_curve()
    plt.plot(roc_curve[0], roc_curve[1])
    with tempfile.TemporaryDirectory() as tempdir:
        temppath = Path(tempdir) / 'roc.png'
        plt.savefig(temppath)
        mlflow.log_artifact(str(temppath))


def save_splits(splits):
    logging.info('Saving splits.')
    with tempfile.TemporaryDirectory() as tempdir:
        temppath = Path(tempdir)
        sales.save_splits(temppath, splits)
        mlflow.log_artifacts(str(temppath), 'data-splits')


def train(input_path, n_estimators, n_iter):
    with mlflow.start_run(experiment_id=EXPERIMENT_ID):
        log_params(input_path, n_estimators, n_iter)
        input_df = pd.read_csv(input_path)
        logging.info(f'Read {input_df.shape[0]} rows of input data.')
        processed_df = sales.transform_target_in_df(input_df)
        logging.info('Preprocessed the data.')
        splits = sales.split_data(processed_df)
        save_splits(splits)
        logging.info(f'Created splits.')
        model = sales.SalesPredictor(n_estimators, n_iter)
        model.fit(splits.train_features, splits.train_target)
        logging.info('Model training complete.')
        mlflow.sklearn.log_model(
            model, 
            'trained-model', 
            serialization_format=mlflow.sklearn.SERIALIZATION_FORMAT_CLOUDPICKLE
        )
        logging.info('Trained model saved.')
        metric_calculator = sales.MetricCalculator(
            model, splits.test_features, splits.test_target)
        log_metrics(metric_calculator)


def main():
    args = create_parser().parse_args()
    train(args.raw, args.estimators, args.iter)
    

if __name__ == '__main__':
    main()
