
import requests

APP_PORT = 8080  # Set this to the correct port

record = {
    "Supplies Subgroup": "Motorcycle Parts",
    "Supplies Group": "Performance & Non-auto",
    "Region": "Pacific",
    "Route To Market": "Reseller",
    "Elapsed Days In Sales Stage": 78,
    "Sales Stage Change Count": 2,
    "Total Days Identified Through Closing": 12,
    "Total Days Identified Through Qualified": 12,
    "Opportunity Amount USD": 25000,
    "Client Size By Revenue": 1,
    "Client Size By Employee Count": 1,
    "Revenue From Client Past Two Years": 0,
    "Competitor Type": "Unknown",
    "Ratio Days Identified To Total Days": 0.0,
    "Ratio Days Validated To Total Days": 1.0,
    "Ratio Days Qualified To Total Days": 0.0,
    "Deal Size Category": 3,
}

response = requests.post(f'http://localhost:{APP_PORT}/predict', json=record)

response.raise_for_status()

print(response.json())
